package com.example.demo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.model.Employee;

public interface PaginationDao extends PagingAndSortingRepository<Employee, Integer> {
}
